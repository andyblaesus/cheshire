'use strict'

const Promise = require('bluebird')
const path = require('path')
const sha1 = require('sha1')
const fs = Promise.promisifyAll(require('fs'))
const url = require('url')
const diff = require('diff')
const beautify = require('js-beautify').js_beautify

const config = require('../config')

let upload
let getRemoteUrl

if (config.storageService === 'qiniu') {
  const qiniu = require('qiniu')
  qiniu.conf.ACCESS_KEY = config.authentication.ACCESS_KEY
  qiniu.conf.SECRET_KEY = config.authentication.SECRET_KEY
  upload = (options) => {
    const bucket = config.remoteDetails.bucket
    const key = options.remoteFilename || Math.random().toString().slice(2)

    // Upload token policy, copied from Qiniu official example
    function makeUploadTokenPolicy(bucket, key) {
      const putPolicy = new qiniu.rs.PutPolicy(bucket+":"+key)
      return putPolicy.token()
    }
    const token = makeUploadTokenPolicy(bucket, key)
    const filePath = options.localFilename

    function uploadFile(uptoken, key, localFilePath) {
      return new Promise((resolve, reject) => {
        const extra = new qiniu.io.PutExtra();
        qiniu.io.putFile(uptoken, key, localFilePath, extra, (error, response) => {
          if (!error) {
            resolve(response)
          } else {
            reject(error)
          }
        })
      })
    }
    return uploadFile(token, key, filePath)
  }

  getRemoteUrl = (filename) => {
    return `http://7xpzkh.com2.z0.glb.qiniucdn.com/${filename}`
  }
} else {
  throw new Error(`Unimplemented storage service: ${config.storageService}`)
}

function rename(prefix, content, previousName) {
  return `${prefix}-${sha1(content)}${path.extname(previousName)}`
}

function save(filename, content) {
  return fs.writeFileAsync(toTemp(filename), content)
}

function prependProtocol(url) {
  if (!url || typeof url !== 'string') {
    return ''
  } else if (url.slice(0, 2) !== '//') {
    return url
  }
  return 'http:' + url
}

function toTemp(filename) {
  return path.join(config.tempDir, filename)
}

function say(text) {
  process.stdout.write(text)
}

function getBaseScriptUploadUrl(baseScriptUrl) {
  return url.parse(baseScriptUrl).path.slice(1) + '~'
}

function prettyDiff(s1, s2) {
  const differences =  diff.diffLines(beautify(s1), beautify(s2))
                           .filter(difference => difference.added || difference.removed)
  return differences.map(d => {
    if (d.added) {
      return `ADD    ${d.value.trim()}`
    } else if (d.removed) {
      return `REMOVE ${d.value.trim()}`
    }
  }).join('\n')
}

module.exports = {upload, prependProtocol, rename, getRemoteUrl, toTemp, save, getBaseScriptUploadUrl, say, prettyDiff}
