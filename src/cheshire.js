/*
  Cheshire - the <del>misdirecting</del> redirecting cat
  It does the following:
  1. grabs a "base script" from an url
  2. look for (predetermined) "external scripts" references in the base script
  3. move the external scripts to a new place
  4. modify base script remotely to source external scripts from new place
*/

'use strict'

const Promise = require('bluebird')
const request = require('request-promise')
const url = require('url')
const path = require('path')
const mkdirp = require('mkdirp')
const rimraf = require('rimraf')
const inquirer = require('inquirer')
const utils = require('./utils')

const config = require('../config')
const baseScriptUrl = config.baseScriptUrl
const externalScriptRecords = config.externalScriptDescriptions

let originalBaseScriptContent = ''
let finalBaseScriptContent = ''

mkdirp.sync(config.tempDir)

utils.say(`This is Cheshire, the redirecting cat.`)
inquirer.prompt([{
  type: 'list',
  name: 'intention',
  message: `
According to the current confing.js, you intend to modify
${config.baseScriptUrl}
And relocate [${config.externalScriptDescriptions.map(script => script.name).join(', ')}] to [${config.storageService}]
Is that correct?`,
  choices: [
    'No, something is wrong',
    'Yes, please continue',
  ]
}])
  .then((answer) => {
    if (answer.intention.toLowerCase()[0] !== 'y') {
      console.log('Bye')
      return
    }
    
    utils.say(`\nDownloading base script from ${baseScriptUrl}...`)
    request(baseScriptUrl)
      .then(baseScriptContent => {
        utils.say(`Done\n`)
        originalBaseScriptContent = finalBaseScriptContent = baseScriptContent
        externalScriptRecords.forEach(target => {
          target.url = utils.prependProtocol(baseScriptContent.match(target.regex)[0])
        })
        return externalScriptRecords.map(target =>
          request({
            uri: target.url,
            gzip: true,
          })
        )
      })
      .then(targetScriptContentPromises => {
        utils.say(`Downloading external scripts...`)
        Promise.all(targetScriptContentPromises)
          .then(targetScriptContents => {
            utils.say('Done\n')
            for (let i = 0; i < targetScriptContents.length; i += 1) {
              externalScriptRecords[i].content = targetScriptContents[i]
            }
            const saveLocalPromises = externalScriptRecords.map(target => {
              target.filename = utils.rename(target.name, target.content, target.url)
              return utils.save(target.filename, target.content)
            })
            utils.say(`Saving external scripts to temporary directory...`)
            return saveLocalPromises
          })
          .then((saveLocalPromises) => {
            Promise.all(saveLocalPromises)
              .then(() => {
                utils.say(`Done\n`)
                const uploadFilePromises = externalScriptRecords.map(target => {
                  utils.say(`- Uploading ${target.url} to ${config.storageService}\n`)
                  return utils.upload({
                    localFilename: utils.toTemp(target.filename),
                    remoteFilename: target.filename,
                  })
                })
                return uploadFilePromises
              })
              .then(uploadFilePromises => {
                Promise.all(uploadFilePromises)
                  .then(() => {
                    utils.say(`All external scripts uploaded to ${config.storageService}\n`)
                    for (let target of externalScriptRecords) {
                      const newUrl = utils.getRemoteUrl(target.filename)
                      finalBaseScriptContent = finalBaseScriptContent.replace(target.regex, newUrl)
                    }

                    // Save base script and upload
                    utils.say('Modifying base script...\n')
                    utils.save(config.finalBaseScriptFilename, finalBaseScriptContent)
                      .then(() => {
                        inquirer.prompt([{
                          type: 'list',
                          name: 'intention',
                          message: `\n\nHere's the diff:\n` +
                          utils.prettyDiff(originalBaseScriptContent, finalBaseScriptContent) +
                          `\nThis will be uploaded with key [${utils.getBaseScriptUploadUrl(baseScriptUrl)}]`,
                          choices: [
                            'No, something is wrong',
                            'Yes, update the base script',
                          ]
                        }])
                          .then(answer => {
                            if (answer.intention[0].toLowerCase() !== 'y') {
                              utils.say('Sorry for the trouble...See you!\n')
                              return
                            }
                            utils.say('Updating base script...\n')
                            utils.upload({
                                remoteFilename: utils.getBaseScriptUploadUrl(baseScriptUrl),
                                localFilename: utils.toTemp(config.finalBaseScriptFilename),
                              })
                              .then(() => {
                                utils.say('Done!\n')
                                rimraf.sync(config.tempDir)
                              })
                          })
                      })
                  })
              })
          })
      })
  })
  .catch(error => {
    console.error(error)
  })
