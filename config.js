const config = {
  
  // What you want to do
  baseScriptUrl: 'https://o0sgccujz.qnssl.com/analytics.js/v1/xPBGAXEVKOJ7HnZa3w51Q8Mw4plpF7vf/analytics.min.js',
  externalScriptDescriptions: [
    {
      name: 'bugsnag',
      regex: /\/\/d2wy8f7a9ursnm\.cloudfront\.net\/bugsnag-2\.min\.js/,
    },
    {
      name: 'vero',
      regex: /\/\/d3qxef4rp70elm\.cloudfront\.net\/m\.js/,
    },
  ],

  // Where do you do it
  storageService: 'qiniu',
  remoteDetails: {
    bucket: 'segment'
  },
  authentication: {
    ACCESS_KEY: process.env.QINIU_ACCESS_KEY || '',
    SECRET_KEY: process.env.QINIU_SECRET_KEY || '',
  },
  
  tempDir: 'cheshire-temp',
  finalBaseScriptFilename: 'final-base-script.js'
}

module.exports = config